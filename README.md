# lost-forgotten-unloved

## Usage
- Install requirements listed in requirements.txt
- (optional) adjust out_file_path in scrape.py to generate a new output file
- run scrape.py

## Logo
<p style="font-size: 0.9rem;font-style: italic;"><a href="https://svgsilh.com/ms/e91e63/image/2789751.html">"labirin"</a> is licensed under <a href="https://creativecommons.org/licenses/cc0/1.0/?ref=ccsearch&atype=html" style="margin-right: 5px;">CC0 1.0</a><a href="https://creativecommons.org/licenses/cc0/1.0/?ref=ccsearch&atype=html" target="_blank" rel="noopener noreferrer" style="display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;"></a></p>
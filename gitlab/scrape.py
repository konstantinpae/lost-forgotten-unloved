import requests
import json
import time

gitlab_url = "https://gitlab.com/api/v4/projects"
url_params = {
    "simple": "true",
    "order_by": "id",
    "last_activity_before": "2015-06-18T00:00:00Z"
}
filtered_json = []
counter = 0
out_file_path = '../raw_data/gitlab-before-2015-06-18.json'

resp = requests.get(url=gitlab_url, params=url_params)

for project in resp.json():
    if project['star_count'] == 0 and project['forks_count'] == 0:
        filtered_json.append(project)

while counter < 50:
    try:
        resp = requests.get(url=resp.links['next']['url'])
        for project in resp.json():
            if project['star_count'] == 0 and project['forks_count'] == 0:
                filtered_json.append(project)
        # sleeping to avoid rate restrictions from the gitlab api
        time.sleep(1)
        counter += 1
    except KeyError:
        print('no more entries')
        break

with open(out_file_path, 'w') as f:
    f.write(json.dumps(filtered_json, indent=4))
